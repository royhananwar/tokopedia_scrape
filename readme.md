# Scrape Tokopedia
Scrape top 100 products from category Handphone from Tokopedia.  
The result of the application is creating the csv file that contains:
1. Name of Product
2. Description
3. Image Link
4. Price
5. Rating (out of 5 stars)
6. Name of store or merchant

## Getting Started
For the initiation to develop or probably re-develop the system, you will need to setup the **environment**. Please follow these steps below.

## Pyenv Environment
### Pyenv Environment Installation
I'm using pyenv virtualenv for the project, so please follow these steps to install
1. `brew install pyenv`
2. `brew install pyenv-virtualenv`
3. add `eval "$(pyenv virtualenv-init -)"` to your shell
   
Make sure pyenv virtualenv is installed or if you need more docs please refer to https://github.com/pyenv/pyenv and https://github.com/pyenv/pyenv-virtualenv

### Python Installation
After you are installing the pyenv, the next step is install the python inside pyenv virtualenv, so please follow these steps to install
1. `pyenv install 3.6.7`
2. `pyenv virtualenv 3.6.7 tokopedia_scrape`
3. `pyenv activate tokopedia_scrape`
4. `python --version`
   
Make sure the result is `Python 3.7.6`, it mean your setup is success

### Google Chrome Instlation
I assume your machine has the google chrome browser.  
You can refer to this https://www.google.com/chrome/ if want to download.

### Clone Repo
You need to clone the repo for running the application, please follow this step
1. `cd ~/`
2. `mkdir tokopedia_scrape && cd tokopedia_scrape`
3. `git clone https://gitlab.com/royhananwar/tokopedia_scrape .`

You can go to the folder and see if the structure of the application


### Settings Up
Inside of the file `config.py` there's a dict called `tokopedia_scraper_config`  
if the current `css selector` is not working, you just need to change the `value` of each `key` provided there


### How to Run
1. Go to project folder `cd ~/tokopedia_scrape`
2. Activate virtualenv `pyenv activate tokopedia_scrape`
3. Install the requirements `pip install requirements.txt`
4. Run the application with `python app.py`
5. If the selenium / chrome browser is open, it mean the scraping is running
6. wait untill everything is finished, and it will create a new file called `Top 100 Handphone Products.csv`



## Virtualenv Environment
### Python Installation
I assume for the macOS or Linux is already installed with python 3.6++  
for windows user please refer to docs https://www.python.org/downloads/windows/

### Google Chrome Instlation
I assume your machine has the google chrome browser.  
You can refer to this https://www.google.com/chrome/ if want to download.

### Clone Repo
You need to clone the repo for running the application, please follow this step
1. `cd ~/`
2. `mkdir tokopedia_scrape && cd tokopedia_scrape`
3. `git clone https://gitlab.com/royhananwar/tokopedia_scrape .`

You can go to the folder and see if the structure of the application

### Virtualenv Installation
Now we are will create the virtualenv for environment project with these command:
1. `python3 -m venv ~/tokopedia_scrape`
2. activate the virtualenv `source tokopedia_scrape/bin/activate`
3. check your python version `python --version` and it must be 3.6++

### Settings Up
Inside of the file `config.py` there's a dict called `tokopedia_scraper_config`  
if the current `css selector` is not working, you just need to change the `value` of each `key` provided there

### How to Run
1. Go to project folder `cd ~/tokopedia_scrape`
2. Activate virtualenv `source bin/activate`
3. Install the requirements `pip install requirements.txt`
4. Run the application with `python app.py`
5. If the selenium / chrome browser is open, it mean the scraping is running
6. wait untill everything is finished, and it will create a new file called `Top 100 Handphone Products.csv`