tokopedia_scraper_config = {
    'base_url': 'https://www.tokopedia.com/p',
    'handphone_endpoint': '/handphone-tablet/handphone',
    'path_chrome_driver': './chromedriver',
    'output_file_name': 'Top 100 Handphone Products.csv',
    'css_selector': {
        'list_page': '[class="css-bk6tzz e1nlzfl2"]',
        'product_url': '[class="css-89jnbj"]',
        'product_name': '[class="css-1bjwylw"]',
        'product_price': '[class="css-o5uqvq"]',
        'product_rating': '[data-testid="lblPDPDetailProductRatingNumber"]',
        'product_description': '[data-testid="lblPDPDescriptionProduk"]',
        'product_merchant': '[data-testid="llbPDPFooterShopName"]',
        'product_image_multiple_url': '[data-testid="PDPImageThumbnail"]',
        'product_image_single_url': '[class="css-1c345mg"]'
    }
}