import time
import os
import csv

from selenium import webdriver
from selenium.webdriver.common.by import By

from config import tokopedia_scraper_config


def get_batch_product(driver):
    """
    Get batch products from category tokopedia, in this case spesific for handphone
    
    It will get the product_url, product_name and product_price

    Args:
        driver (webdriver): webdriver from selenium

    Returns:
        product_list (list(dict)): product list from category url
    
    Example:
        product_list = [
            {
                "product_url": "https://www.tokopedia.com/xiaomi/xiaomi-redmi-note-10-pro-8-128gb-nfc-6-67-108mp-5020mah-smart-hp-gradient-bronze?src=topads",
                "product_name": "Xiaomi Redmi Note 10 Pro 8/128GB NFC 6.67" 108MP 5020mAh Smart HP - Gradient Bronze",
                "product_price": "Rp3.899.000"
            },
            {
                "product_url": "https://www.tokopedia.com/supergadgettt/samsung-galaxy-a52-8-128-8-256-gb-kamera-4k-ram-8-rom-128-256-resmi-awesome-black-a52-8-128-gb?whid=0",
                "product_name": "Samsung Galaxy A52 8/128 8/256 GB Kamera 4K RAM 8 ROM 128 256 Resmi - Awesome Black, A52 8/128 GB",
                "product_price": "Rp4.880.000"
            }
        ]
    """
    product_list = []
    for page_number in range(1, 10): # assume max is 240 data
        url_to_scrape = '{}{}?page={}'.format(tokopedia_scraper_config['base_url'], tokopedia_scraper_config['handphone_endpoint'], page_number)
        driver.get(url_to_scrape)
        driver.execute_script("window.scrollTo(0, 2000)") 
        # wait 5 seconds to make sure the page rendered
        time.sleep(5)
        
        # get the products by css selector
        rendered_page = driver.find_elements(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['list_page'])
        
        # manipulate product and get the data it's needed
        for result in rendered_page:
            detail = {}
            product_url = result.find_element(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['product_url']).get_attribute("href") # get the url from href
            product_name = result.find_element(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['product_name']).text 
            product_price = result.find_element(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['product_price']).text
            
            # add the detail product into dict so it can easy to manipulate
            detail['product_url'] = product_url
            detail['product_name'] = product_name
            detail['product_price'] = product_price
            
            product_list.append(detail)
    
    return product_list


def get_detail_product(driver, products):
    """
    Get detail product based on product_url from get_batch_product()
    
    it will get all of the information needed such as product_rating, product_description, product_merchant and product_images
    
    Some of the url can't be accessed because getting denied from tokopedia itself, so if the response is denied it will continue
    to the next page and not write the denied product
    
    NOTE: product_image_url separated by ';'

    Args:
        driver (webdriver): webdriver from selenium
        products (list(object)): products from get_batch_product()

    Returns:
        result (list(dict)): product details
    
    Example:
    result = [
        {
            "product_name": "Iphone XR Garansi Resmi Ibox",
            "product_price": "Rp3.899.000",
            "product_rating": "4 (out of 5 stars)",
            "product_description": "product description,
            "product_merchant": "Apple",
            "product_image_url": "https://images.tokopedia.net/img/cache/100-square/VqbcmM/2021/11/10/322e5332-0e8b-44bb-b9b8-837ccead1282.png.webp?ect=4g;https://images.tokopedia.net/img/cache/100-square/VqbcmM/2021/11/10/b4e42c19-1140-4abf-9293-205ffa46a582.png.webp?ect=4g"
        }
    ]
    """
    results = []
    for product in products:
        detail = {}
        url_links = []
        
        # check if the result is 100 then it will stop processing the rest of the url
        if len(results) == 100:
            break
        
        if 'https://ta.tokopedia.com/promo/v1/' in product['product_url']:
            continue

        # get the product page and wait 5 seconds till everything get rendered
        driver.get(product['product_url'])
        time.sleep(5)
        
        # if the url is access denied, it will not get the product rating, product description or product merchant
        # the try exception is neede because if the url is denied it will give default value empty string
        try:
            product_rating = driver.find_element(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['product_rating']).text
        except Exception:
            product_rating = ''
        
        try:
            product_description = driver.find_element(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['product_description']).text.replace('\n', '<br />')
        except Exception:
            product_description = ''
        
        try:
            product_merchant = driver.find_element(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['product_merchant']).text
        except Exception:
            product_merchant = ''
        
        # the skip process is here to check those 3 variable is filled
        if not product_rating and not product_description and not product_merchant:
            print("PRODUCT {} is skipped".format(product['product_url']))
            continue
        
        product_images = driver.find_elements(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['product_image_multiple_url'])
        
        # product images is a list then get every possible image
        for image in product_images:
            url_link = image.find_element(By.TAG_NAME, 'img').get_attribute("src")
            
            # remove the base64 image
            if 'data:image/png;base64' in url_link:
                continue

            url_link = url_link.replace('100-square', '500-square')
            url_links.append(url_link)

        # check if get multiple image is fail, then get the showed image
        if len(url_links) == 0:
            image = driver.find_element(By.CSS_SELECTOR, tokopedia_scraper_config['css_selector']['product_image_single_url'])
            url_link = image.find_element(By.TAG_NAME, 'img').get_attribute("src")
            url_links.append(url_link)

        # mapping the products here to make it easier when writting to the csv
        detail['product_name'] = product['product_name']
        detail['product_price'] = product['product_price']
        detail['product_rating'] = "{} (out of 5 stars)".format(product_rating)
        detail['product_description'] = product_description
        detail['product_merchant'] = product_merchant
        detail['product_image_url'] = ';'.join(url_links)

        results.append(detail)
    
    return results


def make_csv(products):
    """
    Write the result to CSV file
    it will delete the old csv file and create a new one each time the function gets excecuted

    Args:
        products (list(dict)): list of detail product from get_detail_product()
    """
    
    # delete the file if it's exists
    if os.path.isfile(tokopedia_scraper_config['output_file_name']):
        os.remove(tokopedia_scraper_config['output_file_name'])
    
    # filednames must be the same with the key of detail products
    fieldnames = [
        'product_name',
        'product_description',
        'product_image_url',
        'product_price',
        'product_rating',
        'product_merchant'
    ]
    
    with open(tokopedia_scraper_config['output_file_name'], "w", encoding="UTF8") as f:
        
        # create the writer to the csv
        writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='|')
        
        # write the header based on the fieldnames
        writer.writeheader()
        
        # write the products to the csv
        # it will thrown an error if the products have different key with the fieldnames
        writer.writerows(products)


def scrape_tokopedia():
    """
    Scrape tokopedia with selenium, because tokopedia use framework js it's quite hard to get the content without waiting each page finished rendered
    that's why selenium is the best way to do it, request-html or bs4 have advantage to be used because some of the url is getting denied and the process stop there
    
    It will result the csv file named "Top 100 Handphone Products.csv"
    
    NOTE: still have issue with headless browser, the result page is different between headless and headon
    """
    driver = webdriver.Chrome('./chromedriver')
    
    products = get_batch_product(driver)
    detail_products = get_detail_product(driver, products)
    driver.close()
    
    make_csv(detail_products)


if __name__ == '__main__':
    scrape_tokopedia()